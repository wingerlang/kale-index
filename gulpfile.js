let gulp = require('gulp');
let exec = require('child_process').exec;
let tsc = require('gulp-typescript');

gulp.task('build-angular-app', (done) => {
    exec('ng build --extract-css', function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        done(err);
    });
});

gulp.task('compile-server-typescript', () => {
    return gulp.src(['src/server/**/*.ts'])
        .pipe(tsc({
            moduleResolution: "node",
            module: "commonjs",
            esModuleInterop: true,
            target: 'es2015',
            skipLibCheck: true,
            lib: ["es2018", "dom"]
        }))
        .pipe(gulp.dest('dist/server/'));
});

gulp.task('compile',
    gulp.series([
        'compile-server-typescript',
        'build-angular-app',
    ]), async () => {

    });

