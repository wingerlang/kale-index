import moment from 'moment';
import chalk from 'chalk';

export const LOG = {
  info,
  error,
};
export default LOG;

function createTimeStamp(): string {
  return moment().format('HH:mm:ss');
}

function createLog(logFn, timestamp, type, ...message): void {
  logFn(`${timestamp} ${type}  `, ...message);
}

function info(...message): void {
  createLog(console.log, chalk.white(createTimeStamp()), chalk.white('INFO'), ...message);
}

function error(...message): void {
  createLog(console.error, createTimeStamp(), 'ERROR', ...message);
}
