import {NextFunction, Request, Response} from 'express';
import express from 'express';
import * as fs from 'fs';
import * as path from 'path';
import { ApiRoute } from './api';
import chalk from 'chalk';
import LOG from './Logger';
import cors from 'cors';

const methodColorMap = {
  GET: chalk.blue,
  POST: chalk.blue,
  DELETE: chalk.red,
  PUT: chalk,
};

export function showPath(req: Request, res: Response, next: NextFunction): void {
  let method = req.method;
  const color = methodColorMap[method];
  if (color) {
    method = color(method);
  }
  LOG.info(`${chalk.yellow(method)} ${req.url}`);
  next();
}

const app = express();

app.use(showPath);
app.use('/api', ApiRoute);
app.use(cors());

// app.use(express.static(path.join(__dirname, '../kaleindex'), {}));

// app.get('*', (request: Request, response) => {
//   fs.readFile(path.join(__dirname, `../kaleindex/index.html`), 'utf8', (err, data) => {
//     response.contentType('text/html');
//     response.send(data);
//   });
// });

app.listen(process.env.PORT || 1337, () => {
    console.log('Server started');
});
