import { Router, Request, Response } from 'express';

const router: Router = Router();

const stocks = [
  {
    id: 'BYND',
    name: 'Beyond Meat',
    price: 600,
    industry: 'food',
  },
  {
    id: 'IMPO',
    name: 'Impossible food',
    price: 1505,
    industry: 'food'
  }
];

router.get('', (request: Request, response: Response) => {
  response.json(stocks);
});

router.get('/:id', (request: Request, response: Response) => {
    const id = request.params.id;
    const stock = stocks.find((x: any) => x.id === id);
    response.json(stock);
});

export const StocksRouter = router;
