import { Router, Request, Response } from 'express';

const router: Router = Router();

const indexStocks = [
  {},
  {}
];

router.get('', (request: Request, response: Response) => {
  response.json(indexStocks);
});

export const IndexRouter = router;
