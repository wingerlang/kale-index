import { Router, Request, Response, NextFunction } from 'express';
import { StocksRouter } from './stocks/stocks';
import { IndexRouter } from './stocks';

const router: Router = Router();

router.use('/stocks', StocksRouter);
router.use('/index', IndexRouter);

router.all('*', (request: Request, response: Response) => {
    response.status(404).send('U ain´t getting jacked shit from here, 404 biatch!');
});

export const ApiRoute = router;
