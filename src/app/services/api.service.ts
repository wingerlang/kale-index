import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const BASE_URL = '/api';

@Injectable()
export class ApiService {
    constructor(private http: HttpClient) {}

    get<T>(url: string): Promise<T> {
        return this.http.get(`${BASE_URL}${url}`, {
            withCredentials: true
        }).toPromise() as Promise<T>;
    }

    post<T>(url: string, data: any): Promise<T> {
        return this.http.post(`${BASE_URL}${url}`, data, {
            withCredentials: true
        }).toPromise() as Promise<T>;
    }

    put<T>(url: string, data: any): Promise<T> {
        return this.http.put(`${BASE_URL}${url}`, data, {
            withCredentials: true
        }).toPromise() as Promise<T>;
    }

    patch<T>(url: string, data: any): Promise<T> {
        return this.http.patch(`${BASE_URL}${url}`, data, {
            withCredentials: true
        }).toPromise() as Promise<T>;
    }

    delete<T>(url: string): Promise<T> {
        return this.http.delete(`${BASE_URL}${url}`, {
            withCredentials: true
        }).toPromise() as Promise<T>;
    }

}
