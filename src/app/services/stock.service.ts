import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {ApiService} from './api.service';
import {Stock} from '../modules/stock/models';

const STOCKS_URL = `/stocks`;


@Injectable()
export class StockService {
  constructor(private api: ApiService) {
  }

  get(id: string): Promise<Stock> {
    return this.api.get(`${STOCKS_URL}/${id}`, ) as Promise<Stock>;
  }

  getAll(): Promise<Stock[]> {
    return this.api.get(STOCKS_URL) as Promise<Stock[]>;
  }
}
