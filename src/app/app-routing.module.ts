import { NgModule } from '@angular/core';
import { IndexComponent } from './modules/index/index.component';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './modules/main/main.component';
import { StocksComponent } from './modules/stocks/stocks.component';
import { StockComponent } from './modules/stock/stock.component';
import { NothingComponent } from './modules/nothing/nothing.component';
import {PerformanceComponent} from './modules/performance/performance.component';

const routes: Routes = [
  { path: '', component: MainComponent },
  { path: 'index', component: IndexComponent },
  { path: 'stocks', component: StocksComponent },
  { path: 'performance', component: PerformanceComponent },
  { path: 'stock/:id', component: StockComponent },
  { path: '**', component: NothingComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
