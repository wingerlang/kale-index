import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { IndexComponent } from './modules/index/index.component';
import { TabMenuModule } from 'primeng/tabmenu';
import { TableModule } from 'primeng/table';
import { AppRoutingModule } from './app-routing.module';
import { MainComponent } from './modules/main/main.component';
import { ApiService } from './services/api.service';
import { HttpClientModule } from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import { StocksComponent } from './modules/stocks/stocks.component';
import { StockComponent } from './modules/stock/stock.component';
import { NothingComponent } from './modules/nothing/nothing.component';
import {StockService} from './services/stock.service';
import { PerformanceComponent } from './modules/performance/performance.component';

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    MainComponent,
    StocksComponent,
    StockComponent,
    NothingComponent,
    PerformanceComponent
  ],
  imports: [
    BrowserModule,
    TabMenuModule,
    AppRoutingModule,
    HttpClientModule,
    TabMenuModule,
    TableModule,
    FormsModule,
    InputTextModule,
    ButtonModule
  ],
  providers: [
    ApiService,
    StockService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
