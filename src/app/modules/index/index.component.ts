import { Component, OnInit } from '@angular/core';

import { ApiService } from '../../services/api.service';
import moment from 'moment';

export interface Stock {
  id: string;
  name: string;
  industry?: string;
}

export interface Distribution {
  id: string;
  name: string;
  currentWeight: number;
  newWeight?: number;
  industry?: string;
  added: string;
}

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  constructor(private api: ApiService) { }

  message: string;
  stocks: Distribution[];

  clonedProducts: { [s: string]: Distribution; } = {};

  async ngOnInit(): Promise<void> {

    this.stocks = [
      {
        id: 'ELSE',
        name: 'Else Nutrition',
        currentWeight: 0.5,
        industry: 'health',
        added: formatDate('2018-06-01'),
      },
      {
        id: 'BYND',
        name: 'Bynd',
        currentWeight: 0.25,
        industry: 'food',
        added: formatDate('2019-01-01'),
      },
      {
        id: 'IMPL',
        name: 'Impossible',
        currentWeight: 0.25,
        industry: 'food',
        added: formatDate('2020-01-01'),
      },
    ];
  }

  calculateTotalWeight(stocks: Distribution[]): number {
    return stocks.reduce((total, cur) => total + cur.currentWeight || 0, 0);
  }
  calculateTotalNewWeight(stocks: Distribution[]): number {
    return stocks.reduce((total, cur) => total + (cur.newWeight || 0), 0);
  }

  newRow(): void {
    this.stocks.push({
      id: 'N/A',
      name: 'no-name',
      currentWeight: 0,
      newWeight: null,
      added: formatDate(new Date())});
  }

  removeStock(stock: Distribution): void {
    removeItem(this.stocks, stock);
  }
}

function formatDate(d: string|Date): string {
  return moment(d).format('YYYY-MM-DD');
}

function removeItem(array: any[], item: any): boolean {
  const index = array.findIndex(e => e.id === item.id);

  if (index > -1) {
    array.splice(index, 1);
  }

  return index >= 0;
}
