import {Stock} from '../stock/models';

export interface IndexStock {
  added: Date;
  stock: Stock;
  weight: number;
}

export interface Index {
  stocks: IndexStock[];
}
