import { Component } from '@angular/core';

@Component({
    template: `<div class="nothing"><img src="/assets/404.png" /></div>`,
    styles: [`.nothing {text-align: center;} img {max-height: 50vh;}`]
})
export class NothingComponent {
    constructor() {}
}
