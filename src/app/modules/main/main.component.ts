import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';

@Component({
    selector: 'app-main',
    templateUrl: './main.component.html'
})
export class MainComponent implements OnInit {
    constructor(private api: ApiService) { }

    message: string;
    async ngOnInit(): Promise<void> {
    }
}
