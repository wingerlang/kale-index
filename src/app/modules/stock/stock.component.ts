import { Component, OnInit } from '@angular/core';
import {Stock} from '../index/index.component';
import {StockService} from '../../services/stock.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.css']
})
export class StockComponent implements OnInit {
  constructor(private stockService: StockService, private route: ActivatedRoute) { }

  stock: Stock;
  async ngOnInit(): Promise<void> {
    const id = this.route.snapshot.paramMap.get('id');
    this.stock = await this.stockService.get(id);
  }
}
