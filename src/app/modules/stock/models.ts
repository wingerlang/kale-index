export interface Price {
  date: Date;
  opening: number;
  close: number;
  // Change? pct change? calculated by opening & close
}

export interface Stock {
  id: string; // ticker
  marketplace?: string; // ID reference for the marketplace
  name: string;
  country: string;
  history: Price[];
}

export interface Index {
  id: string;
  name: string;
  composition: any[];
}

export interface IndexPart {
  stock: Stock;
  date: Date;
}
