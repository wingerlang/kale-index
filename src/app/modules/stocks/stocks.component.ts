import { Component, OnInit } from '@angular/core';
import {Stock} from '../index/index.component';
import {StockService} from '../../services/stock.service';

@Component({
  selector: 'app-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css']
})
export class StocksComponent implements OnInit {
  constructor(private stockService: StockService) { }

  stocks: Stock[];

  async ngOnInit(): Promise<void> {
    this.stocks = await this.stockService.getAll();
  }

  newRow(): void {
    this.stocks.push({
      id: '',
      name: ''
    });
  }
}
