import {Component, OnInit} from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css',
    '/node_modules/primeng/resources/themes/saga-blue/theme.css',
    '/node_modules/primeng/resources/primeng.min.css',
    '/node_modules/primeicons/primeicons.css']
})

export class AppComponent implements OnInit {
  title = 'kaleindex';
  items: MenuItem[] = [];

  ngOnInit(): void {
    this.items = [
      { label: 'Home', icon: 'pi pi-fw pi-home', routerLink: ['/'] },
      { label: 'index', icon: 'pi pi-fw pi-calendar', routerLink: ['/index'] },
      { label: 'Stocks', icon: 'pi pi-fw pi-calendar', routerLink: ['/stocks'] },
      { label: 'Performance', icon: 'pi pi-fw pi-calendar', routerLink: ['/performance'] },
    ];
  }
}
